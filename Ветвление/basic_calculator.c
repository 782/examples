#include <stdio.h>
#include <stdlib.h>

int main(){
    system("chcp 65001");

    float a, b;
    char dec;

    printf("Введите число 1: ");
    scanf("%f", &a);
    printf("Введите число 2: ");
    scanf("%f", &b);
    printf("Введите действие(+,-,*,/): ");
    scanf(" %c", &dec);

    if(dec == '+'){
        printf("Результат:\n %.2f %c %.2f = %.2f", a, dec, b, a+b);
    }
    else if(dec == '-'){
        printf("Результат:\n %.2f %c %.2f = %.2f", a, dec, b, a-b);
    }
    else if(dec == '*'){
        printf("Результат:\n %.2f %c %.2f = %.2f", a, dec, b, a*b);
    }
    else if(dec == '/' && b!=0){
        printf("Результат:\n %.2f %c %.2f = %.2f", a, dec, b, a/b);
    }
    else if (b==0 && dec == '/'){
        printf("Результат:\n %.2f %c %.2f невозможен", a, dec, b);
    }
}