#include <stdio.h>
#include <stdlib.h>
#include <math.h>
// f(x) = e^x (M_E^x)
int main(){
    system("chcp 65001");
    float x, y;
    printf("Введите х: ");
    scanf("%f", &x);
    printf("\nВведите y: ");
    scanf("%f", &y);

    float result;
    float f = pow(M_E, x);
    result = f+y;
    result = pow(result, 2);

    if(x*y>0){
        result -= cbrt(abs(f));
    }
    else if(x*y<0){
        result += sin(x);
    }
    else if(x*y == 0){
        result += pow(y, 3);
    }
    else{
        printf("Что-то пошло не так. Перезапустите программу");
        return 1;
    }

    printf("Результат вычислений:\n\ta = %.2f", result);
    return 0;
}