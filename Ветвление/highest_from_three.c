#include <stdio.h>
#include <stdlib.h> // system()

// Дано 2 числа. Вывести наибольшее

int main(){
    system("chcp 65001"); // UTF-8
    float a, b, c;

    // Ввод чисел
    printf("\nВведите число а: ");
    scanf("%f", &a);
    printf("\nВведите число b: ");
    scanf("%f", &b);
    printf("\nВведите число c: ");
    scanf("%f", &c);

    // Поиск наибольшего из трёх
    if(a>b && a>c){
        printf("\nЧисло a больше чисел b и c.");
    }
    if (b>a && b>c){
        printf("\nЧисло b больше чисел a и с.");
    }
    if (c>a && c>b){
        printf("\nЧисло c больше чисел a и b.");
    }
    return 0;
}