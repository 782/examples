#include <stdio.h>
#include <stdlib.h>

int main(){
    system("chcp 65001");
    printf("\t\tУРАВНЕНИЕ ВИДА y=kx+b\n");
    
    float start, finish, step;
    printf("Введите координату X начала: ");
    scanf("%f", &start);
    printf("Введите координату X конца: ");
    scanf("%f", &finish);
    printf("Введите шаг: ");
    scanf("%f", &step);

    float k, b;
    printf("Введите угловой коэффициент k: ");
    scanf("%f", &k);
    printf("Введите свободный член b: ");
    scanf("%f", &b);
    
    printf("Y\tX\n");
    for(float i = start; i <= finish; i+=step){
        float y = k*i + b;
        printf("%3.2f\t%3.2f\n", y, i); 
    }
}