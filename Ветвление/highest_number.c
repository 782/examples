#include <stdio.h>
#include <stdlib.h> // system()

// Дано 2 числа. Вывести наибольшее

int main(){
    system("chcp 65001"); // UTF-8
    double a, b;

    // Ввод чисел
    printf("\nВведите число а: ");
    scanf("%f", &a);
    printf("\nВведите число b: ");
    scanf("%f", &b);

    // Поиск наибольшего из двух
    if(a>b){
        printf("\nЧисло a больше числа b.");
    }
    else{
        printf("\nЧисло b больше числа a.");
    }
    return 0;
}