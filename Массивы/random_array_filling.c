#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int random_filling(int *array, int array_length){
    for(int i = 0; i < array_length; i++){
        array[i] = rand()%(array_length*5)+1;
    }
}

int main(){
    system("chcp 65001");
    srand(time(NULL));
    printf("Enter array length: ");
    int n;
    scanf("%d", &n);
    int array[n];

    random_filling(array, n);

    printf("Generated array: ");
    for(int i = 0;i < n; i++){
        printf("%d ", array[i]);
    }
}