#include <stdio.h>
#include <stdlib.h>

int main(){
    system("chcp 65001");

    float n;
    char dec;

    printf("Введите число 1: ");
    scanf("%f", &n);
    printf("Введите начальное число: ");
    float start;
    scanf("%f", &start);

    printf("Введите действие(+,-,*,/): ");
    scanf(" %c", &dec);

    if(start<n){
        float res;
        if(dec == '+'){
            res = 0;
            for(float i = start; i<=n;i++){
                res += i;
            }
        }
        else if(dec == '-'){
            res = 0;
            for(float i = start; i<=n;i++){
                res -= i;
            }
        }
        else if(dec == '*'){
            res = 1;
            for(float i = start; i<=n;i++){
                res += i;
            }
        }
        
        else if (dec == '/'){
            res = 1;
            for(float i = start; i<=n;i++){
                res += i;
            }
        }
        printf("Результат: %.2f", res);
    }
    else{
        printf("Начальное число не может быть больше конечного. Попробуйте еще раз.");
    }
}