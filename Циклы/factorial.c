#include <stdio.h>
#include <stdlib.h>

int main(){
    system("chcp 65001");
    int n;
    printf("Введите число N: ");
    scanf("%d", &n);
    int factorial = 1;
    if(n<0){
        printf("%d! невозможен", n);
    }
    else{
        for(int i = 1; i<=n;i++){
            factorial*=i;
        }
        printf("%d! = %d", n, factorial);
    }
}